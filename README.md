hello world!

# Mon premier dépôt Git

Ceci est mon premier dépôt.

## Liste des commandes
- `git init` : initialise le dépôt
- `git add` : ajoute un fichier à la zone d'index
- `git commit` : valide les modifications indexées dans la zone d'index
- `git status` : voir l'etat du depot
- `git log` : afficher l'historique
-